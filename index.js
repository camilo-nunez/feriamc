const Discord = require('discord.js');
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const { prefix,token } = require('./config.json');

var last_user;

// Discord init funtion's call
const client = new Discord.Client();
client.login(token);
client.once('ready', () => {
	console.log('Ready!');
});

//Google api configures
const SCOPES = ['https://www.googleapis.com/auth/calendar'];
const TOKEN_PATH = 'token.json';

function authorize(credentials, callback, message) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);
  
    fs.readFile(TOKEN_PATH, (err, token) => {
      if (err) return getAccessToken(oAuth2Client, callback, message);
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client, message);
    });
}

function getAccessToken(oAuth2Client, callback, message) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err) return console.error('Error retrieving access token', err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
          if (err) return console.error(err);
          console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client, message);
      });
    });
}

// Google api functions discord
function insertEvents(auth, message) {

    const args = message.content.slice(prefix.length).split(' ');
    const command = args.shift().toLowerCase();

    // console.log(args)
 
    var start_date = new Date(args[0]); //YYYY-MM-DDThh:mm:ss https://www.w3.org/TR/NOTE-datetime
    var end_date = new Date(args[0]);
    end_date.setHours(end_date.getHours() + 2);

    var summary = "Reunion de feria 2020"
    var description = "Reunion de feria 2020"


    const calendar = google.calendar({ version: 'v3', auth });
    var event = {
      summary: summary,
      location: 'En el mismisimo Discord',
      description: description,
      start: {
        // dateTime: '2020-04-01T01:00:00-07:00',
        dateTime: start_date.toISOString(),
        timeZone: 'America/Santiago'
      },
      end: {
        // dateTime: '2020-04-01T17:00:00-07:00',
        dateTime: end_date.toISOString(),
        timeZone: 'America/Santiago'
      },
    //   recurrence: ['RRULE:FREQ=DAILY;COUNT=2'],
      attendees: [{ email: 'nicolas.rosasg@sansano.usm.cl' }, { email: 'nicolas.acevedoy@sansano.usm.cl' }, { email: 'eliecer.zambrano@sansano.usm.cl'}, { email: 'bryan.salas@sansano.usm.cl' }, { email: 'camilo.nunezf@sansano.usm.cl' }],
      reminders: {
        useDefault: false,
        overrides: [
          { method: 'email', minutes: 5 },
          { method: 'popup', minutes: 5 }
        ]
      }
    };
  
    calendar.events.insert(
      {
        auth: auth,
        calendarId: 'primary',
        resource: event,
        'sendUpdates' : 'all'
      },
      function(err, event) {
        if (err) {
          message.channel.send(`There was an error contacting the Calendar service: ${err}`);
          console.log(
            'There was an error contacting the Calendar service: ' + err
          );
          return;
        }
        message.channel.send(`Reunion agendada con exito para el dia : ${event.data.start.dateTime} y su id es: ${event.data.id}`);
        // console.log(event);
        // message.channel.send(`Reunion agendada con exito para el dia : ${start_date.toISOString()}`);
      }
    );
}

// Discord command fuctions
async function randomMC(message){
    message.guild.members.fetch().then(fetchedMembers => {	
        const userRandom = fetchedMembers.filter(member => !member.user.bot).random().user.id;
        message.channel.send(`El próximo MC será ${client.users.cache.get(userRandom)}, F bro :c`);
    });
}

async function scheduleMC(message){
    fs.readFile('credentials.json', (err, content) => {
        if (err) return message.channel.send(`Error loading client secret file:, ${err}!`);
        authorize(JSON.parse(content), insertEvents, message);
    });
}

// Discord bot
client.on('message', async message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(' ');
    const command = args.shift().toLowerCase();

    console.log(`Incomming message from ${message.author} with the command ${command} and arguments ${args}`);

    if (command === 'mc') randomMC(message);

    else if (command === 'agendar') {
        if (!args.length) return message.channel.send(`No proporcionaste ningún argumento, ${message.author}!`);
        else scheduleMC(message);
    }
    else return message.channel.send(`No proporcionaste ningún comando válido , ${message.author}!`);
});